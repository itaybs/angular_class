import { Component, OnInit } from '@angular/core';
import { InvoicesService } from './invoices.service';

@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
  styles: [`
     .invoices li { cursor: default; }
    .invoices li:hover { background: #ecf0f1; } 
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }
  `]
})
export class InvoicesComponent implements OnInit {
 
  invoices;
  currentInvoices;
  
  select(invoice){
    this. currentInvoices = invoice;
  }

  addinvoice(invoice){
     this._invoicesService.addinvoice(invoice);//add invoice through service file where there is an add invoice function.
  }
  constructor(private _invoicesService:InvoicesService) { }

  ngOnInit() {
    this._invoicesService.getInvoices().subscribe(invoicesData => this.invoices = invoicesData);

}

}
