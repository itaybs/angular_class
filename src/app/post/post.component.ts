import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Post } from './post';
@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
  edit: Boolean=false;
  post:Post;
  isEdit : boolean = false;//start with is edit button is false (not editing)
  editButtonText = 'Edit';//we will se Edit on button
  @Output('title') oldtitle: string;//create variable of title
  @Output('body') oldbody: string;//create variable of body
  @Output() editEvent = new EventEmitter<Post>();//create new evnet of editing
    @Output() deleteEvent = new EventEmitter<Post>();
  constructor() { }

  toggleEdit(){
     this.oldtitle = this.post.title;
     this.oldbody = this.post.body;
     //update parent about the change
     this.isEdit = !this.isEdit; //for toggle property, isEdit can be true or false and not(!) will be other way
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
     this.isEdit ?  this.edit= true : this.edit= false;
     if (!this.isEdit)
     this.editEvent.emit(this.post);  
  }
  cancelEdit(){
this.post.title=  this.oldtitle;
this.post.body=  this.oldbody;
this.toggleEdit();
this.edit=false;
}
sendDelete(){
this.deleteEvent.emit(this.post);

}
  ngOnInit() {
  }

}