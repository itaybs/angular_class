import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Invoice } from './invoice';

@Component({
  selector: 'jce-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  inputs:['invoice']
})
export class InvoiceComponent implements OnInit {
 edit: Boolean=false;
  invoice:Invoice;
  isEdit : boolean = false;//start with is edit button is false (not editing)
  editButtonText = 'Edit';//we will se Edit on button
  @Output('name') oldname: string;//create variable of name
  @Output('amount') oldamount: string;//create variable of body
  @Output() editEvent = new EventEmitter<Invoice>();//create new evnet of editing
    @Output() deleteEvent = new EventEmitter<Invoice>();

  constructor() {
         this.oldname = this.invoice.name;
     this.oldamount = this.invoice.amount;
     //update parent about the change
     this.isEdit = !this.isEdit; //for toggle property, isEdit can be true or false and not(!) will be other way
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
     this.isEdit ?  this.edit= true : this.edit= false;
     if (!this.isEdit)
     this.editEvent.emit(this.invoice);
   }





  ngOnInit() {
  }

}
