import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';//function that delays loading of data

@Injectable()
export class InvoicesService {
  //  private _url='http://jsonplaceholder.typicode.com/invoices';
invoicesObservable;
  getInvoices (){
    this.invoicesObservable = this.af.database.list('/invoices').map(
       invoices => {
        invoices.map(
          invoice=> {
            invoice.invoiceNames = [];
          }
        );
        return invoices;
      }
    );
    return this.invoicesObservable;
//return this._http.get(this._url).map(res => res.json()).delay(2000)
  }
  


    addinvoice(invoice){
    this.invoicesObservable.push(invoice);//push is a function on array, adds object in end of array.
}
// updateinvoice(invoice){
// let invoiceKey = invoice.$key;
//let invoiceData = {name:invoice.name,email:invoice.email};
//this.af.database.object('/invoices/' + invoiceKey).update(invoiceData);
//}
//  deleteinvoice(invoice){
//let invoiceKey = invoice.$key;
//this.af.database.object('/invoices/' + invoiceKey).remove();    
 //  }
  constructor(private af:AngularFire) { }
 
}
